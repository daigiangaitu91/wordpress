<p>
    <label for="<?php echo $this->get_field_id('title') ?>"> <?php echo _e('Widget title','tp_weather') ?> :</label>
    <input class="widgetfat" id="<?php echo $this->get_field_id('title') ?>" name="<?php echo $this->get_field_name('title') ?>" type="text" value="<?php echo esc_attr($title) ?>" > 
</p>
<p>
    <label for = "<?php echo $this->get_field_id('unit') ?>"><?php echo _e('Unit','tp_weather') ?>:</label>
<select class="widefat" id="<?php echo $this->get_field_id('unit') ?>" name ="<?php echo $this->get_field_name('unit') ?>" >
<option value = "fahrenheit" <?php echo ($unit == 'fahrenheit') ? 'selected' : '' ?> >Fahrenheit</option>
<option value = "celsius" <?php echo ($unit == 'celsius') ? 'selected' : '' ?>>Celsius</option>
</select>
</p>	