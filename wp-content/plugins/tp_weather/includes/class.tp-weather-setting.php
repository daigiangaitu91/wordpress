<?php
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

class TP_Weather_Setting {
	protected $option;
	protected $option_group = 'tp_weather_group';

	public function __construct() {
		 $this->option = get_option('tp_weather_setting');
		//Add Menu
		
	}

	public function create_page() {
		$option_group = $this->option_group;
		
		require(TP_WEATHER_PLUGIN_DIR . 'views/tp-weather-widget-setting.php');
	}

	public function register_settings(){
		register_setting(
			$this->option_group,
			'tp_weather_setting',
			array($this,'save_setting')
			);
	}
	public function save_setting($input){
		$new_input = array();
		if (isset($input['city_name']) && !empty($input['city_name'])) {
			foreach ( $input['city_name'] as $value) {
				$new_input['city_name'][] = preg_replace('/[ ]/u', '+', trim($value));
			}
		} else {
			$new_input['city_name'][] = 'Ho+Chi+Minh'; 
		}
		return $new_input;
	}
	public function search_city_ajax(){
		if (isset($_POST['city']) && !empty($_POST['city'])) {
			$data = TP_Weather_API::request($_POST['city']);
			wp_send_json_success($data);
		}
	}
}
function add_submneu_setting(){
	$tp_weather_setting = new TP_Weather_Setting();
		add_submenu_page(
				'options-general.php',
				'TP Weather Settings',
				'TP Settings',
				'manage_options',
				'tp_weather',
				array($tp_weather_setting, 'create_page')
			);
	}

function register_scripts(){
	
			wp_register_script('tp-js', TP_WEATHER_PLUGIN_URL . 'scripts/js/functions.js', ['jquery']);
			wp_localize_script('tp-js', 'tp', [
				'url' => admin_url('admin-ajax.php')
			]);
			wp_enqueue_script('tp-js');
}
$tp_weather_setting = new TP_Weather_Setting();
add_action('admin_init',array($tp_weather_setting,'register_settings'));
add_action('admin_enqueue_scripts','register_scripts');
add_action('wp_ajax_search_city_ajax',array($tp_weather_setting,'search_city_ajax'));
add_action('admin_menu','add_submneu_setting');
