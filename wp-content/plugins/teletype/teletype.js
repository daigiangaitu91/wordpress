( function ( $ ) {
	$( function() {
		$( window.teletype.selector ).each( function() {
			var items = $( this ).html()
				.replace( /(<\/p>)/gi, "\n" )
				.replace( /(<([^>]+)>)/ig, '' )
				.split( "\n\n" );
			$( this ).empty().teletype(
				$.extend( { text: $.map( items, $.trim ) }, window.teletype )
			);
		} );
	} );
}( jQuery ) );