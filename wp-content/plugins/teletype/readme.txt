=== Teletype ===
Contributors: CatN, StvWhtly
Tags: jquery, teletype, type, console
Requires at least: 4.3
Tested up to: 4.3
Stable tag: 0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Integrates the jQuery Teletype Plugin into your WordPress install.

== Description ==

Teletype is a jQuery plugin that types out text, and then optionally deletes it, replicating human interaction.

Additional options provide the ability to preserve the typed text, in a console / terminal format, pause during typing and delete characters.

An online demo can be found at http://teletype.rocks/.

== Installation ==

1. Upload the `teletype` folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Use the shortcode `[teletype]` in your posts or pages.

== Changelog ==

= 0.1 =
This is the first release of the plugin.
