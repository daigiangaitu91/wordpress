<?php
/*
Plugin Name: Teletype
Plugin URI: https://catn.com/2014/09/09/writing-a-simple-wordpress-plugin-from-scratch/
Description: Integrates the jQuery Teletype Plugin into your WordPress install.
Version: 0.1
Author: StvWhtly
Author URI: http://catn.com/
*/

defined( 'ABSPATH' ) or die( 'Plugin file cannot be accessed directly.' );

if ( ! class_exists( 'Teletype' ) ) {
	class Teletype
	{
		/**
		 * Tag identifier used by file includes and selector attributes.
		 * @var string
		 */
		protected $tag = 'teletype';

		/**
		 * User friendly name used to identify the plugin.
		 * @var string
		 */
		protected $name = 'Teletype';

		/**
		 * Current version of the plugin.
		 * @var string
		 */
		protected $version = '0.1';

		/**
		 * List of options to determine plugin behaviour.
		 * @var array
		 */
		protected $options = array();

		/**
		 * List of settings displayed on the admin settings page.
		 * @var array
		 */
		protected $settings = array(
			'typeDelay' => array(
				'description' => 'Minimum delay, in ms, between typing characters.',
				'validator' => 'numeric',
				'placeholder' => 100
			),
			'backDelay' => array(
				'description' => 'Minimum delay, in ms, between deleting characters.',
				'validator' => 'numeric',
				'placeholder' => 50
			),
			'blinkSpeed' => array(
				'description' => 'Interval, in ms, that the cursor will flash.',
				'validator' => 'numeric',
				'placeholder' => 1000
			),
			'cursor' => array(
				'description' => 'Character used to represent the cursor.',
				'placeholder' => '|'
			),
			'delay' => array(
				'description' => 'Time in ms to pause before deleting the current text.',
				'validator' => 'numeric',
				'placeholder' => 2000
			),
			'preserve' => array(
				'description' => 'Prevent auto delete of the current string and begin outputting the next string.',
				'type' => 'checkbox'
			),
			'prefix' => array(
				'description' => 'Begin each string with this prefix value.',
			),
			'loop' => array(
				'description' => 'Number of times to loop through the output strings, for unlimited use 0.',
				'validator' => 'numeric',
				'placeholder' => 0
			),
			'humanise' => array(
				'description' => 'Add a random delay before each character to represent human interaction.',
				'type' => 'checkbox',
				'default' => true
			)
		);

		/**
		 * Initiate the plugin by setting the default values and assigning any
		 * required actions and filters.
		 *
		 * @access public
		 */
		public function __construct()
		{
			if ( $options = get_option( $this->tag ) ) {
				$this->options = $options;
			}
			add_shortcode( $this->tag, array( &$this, 'shortcode' ) );
			if ( is_admin() ) {
				add_action( 'admin_init', array( &$this, 'settings' ) );
			}
		}

		/**
		 * Allow the teletype shortcode to be used.
		 *
		 * @access public
		 * @param array $atts
		 * @param string $content
		 * @return string
		 */
		public function shortcode( $atts, $content = null )
		{
			extract( shortcode_atts( array(
				'height' => false,
				'class' => false
			), $atts ) );
	 // Enqueue the required styles and scripts...
			$this->_enqueue();
	 // Add custom styles...
			$styles = array();
			if ( is_numeric( $height ) ) {
				$styles[] = esc_attr( 'height: ' . $height . 'px;' );
			}
	 // Build the list of class names...
			$classes = array(
				$this->tag
			);
			if ( !empty( $class ) ) {
				$classes[] = esc_attr( $class );
			}
	 // Output the terminal...
			ob_start();
			?><pre class="<?php esc_attr_e( implode( ' ', $classes ) ); ?>"<?php
				echo ( count( $styles ) > 0 ? ' style="' . implode( ' ', $styles ) . '"' : '' );
			?>><p><?php echo $content; ?></p></pre><?php
			return ob_get_clean();
		}

		/**
		 * Add the setting fields to the Reading settings page.
		 *
		 * @access public
		 */
		public function settings()
		{
			$section = 'reading';
			add_settings_section(
				$this->tag . '_settings_section',
				$this->name . ' Settings',
				function () {
					echo '<p>Configuration options for the ' . esc_html( $this->name ) . ' plugin.</p>';
				},
				$section
			);
			foreach ( $this->settings AS $id => $options ) {
				$options['id'] = $id;
				add_settings_field(
					$this->tag . '_' . $id . '_settings',
					$id,
					array( &$this, 'settings_field' ),
					$section,
					$this->tag . '_settings_section',
					$options
				);
			}
			register_setting(
				$section,
				$this->tag,
				array( &$this, 'settings_validate' )
			);
		}

		/**
		 * Append a settings field to the the fields section.
		 *
		 * @access public
		 * @param array $args
		 */
		public function settings_field( array $options = array() )
		{
			$atts = array(
				'id' => $this->tag . '_' . $options['id'],
				'name' => $this->tag . '[' . $options['id'] . ']',
				'type' => ( isset( $options['type'] ) ? $options['type'] : 'text' ),
				'class' => 'small-text',
				'value' => ( array_key_exists( 'default', $options ) ? $options['default'] : null )
			);
			if ( isset( $this->options[$options['id']] ) ) {
				$atts['value'] = $this->options[$options['id']];
			}
			if ( isset( $options['placeholder'] ) ) {
				$atts['placeholder'] = $options['placeholder'];
			}
			if ( isset( $options['type'] ) && $options['type'] == 'checkbox' ) {
				if ( $atts['value'] ) {
					$atts['checked'] = 'checked';
				}
				$atts['value'] = true;
			}
			array_walk( $atts, function( &$item, $key ) {
				$item = esc_attr( $key ) . '="' . esc_attr( $item ) . '"';
			} );
			?>
			<label>
				<input <?php echo implode( ' ', $atts ); ?> />
				<?php if ( array_key_exists( 'description', $options ) ) : ?>
				<?php esc_html_e( $options['description'] ); ?>
				<?php endif; ?>
			</label>
			<?php
		}

		/**
		 * Validate the settings saved.
		 *
		 * @access public
		 * @param array $input
		 * @return array
		 */
		public function settings_validate( $input )
		{
			$errors = array();
			foreach ( $input AS $key => $value ) {
				if ( $value == '' ) {
					unset( $input[$key] );
					continue;
				}
				$validator = false;
				if ( isset( $this->settings[$key]['validator'] ) ) {
					$validator = $this->settings[$key]['validator'];
				}
				switch ( $validator ) {
					case 'numeric':
						if ( is_numeric( $value ) ) {
							$input[$key] = intval( $value );
						} else {
							$errors[] = $key . ' must be a numeric value.';
							unset( $input[$key] );
						}
					break;
					default:
						 $input[$key] = strip_tags( $value );
					break;
				}
			}
			if ( count( $errors ) > 0 ) {
				add_settings_error(
					$this->tag,
					$this->tag,
					implode( '<br />', $errors ),
					'error'
				);
			}
			return $input;
		}

		/**
		 * Enqueue the required scripts and styles, only if they have not
		 * previously been queued.
		 *
		 * @access public
		 */
		protected function _enqueue()
		{
	 // Define the URL path to the plugin...
			$plugin_path = plugin_dir_url( __FILE__ );
	 // Enqueue the styles in they are not already...
			if ( !wp_style_is( $this->tag, 'enqueued' ) ) {
				wp_enqueue_style(
					$this->tag,
					$plugin_path . 'teletype.css',
					array(),
					$this->version
				);
			}
	 // Enqueue the scripts if not already...
			if ( !wp_script_is( $this->tag, 'enqueued' ) ) {
				wp_enqueue_script( 'jquery' );
				wp_enqueue_script(
					'jquery-' . $this->tag,
					$plugin_path . 'jquery.teletype.min.js',
					array( 'jquery' ),
					'0.1.2'
				);
				wp_register_script(
					$this->tag,
					$plugin_path . 'teletype.js',
					array( 'jquery-' . $this->tag ),
					$this->version
				);
	 // Make the options available to JavaScript...
	 			$options = array_merge( array(
					'selector' => '.' . $this->tag
				), $this->options );
				wp_localize_script( $this->tag, $this->tag, $options );
				wp_enqueue_script( $this->tag );
			}
		}

	}
	new Teletype;
}
