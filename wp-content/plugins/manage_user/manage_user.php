<?php
/*
Plugin Name: Manage_user
Plugin URI: http://daigiangaitu.wordpress.com
Description: Manage user
Version: 1.0.4
Author: Nhat Khoa
Author URI: http://daigiangaitu.wordpress.com
License: GPLv2 or later
Text Domain: daigiangaitu.wordpress.com
*/
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}
//tao gia tri mac dinh
define ("MANAGE_USER_VERSION" , '1.0.4');
define ("MANAGE_USER_MINIUM_VERSION" , '4.1.1');
define ("MANAGE_USER_PLUGIN_URL" , plugin_dir_url(__FILE__));
define ("MANAGE_USER_PLUGIN_DIR" , plugin_dir_path(__FILE__));
// includes file
require_once(MANAGE_USER_PLUGIN_DIR .'includes/class.manage-user-setting.php');
require_once(MANAGE_USER_PLUGIN_DIR .'includes/class.manage-user-sendmail.php');
require_once(MANAGE_USER_PLUGIN_DIR .'includes/class.manage-user.php');
//khoi tao plugin
$manage_user = new Manage_User();