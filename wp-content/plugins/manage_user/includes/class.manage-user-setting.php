<?php
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

class Manage_User_Setting {
	
	protected $option;
	protected $option_group = 'zipcode_group';

	public function __construct() {
		 $this->option = get_option('zipcode_name');
		
	}
	public function create_page_manage_user() {
		$this->save_zipcode();
		require(MANAGE_USER_PLUGIN_DIR . 'views/manage-user-setting.php');
	}
	public function save_zipcode(){
		if (get_transient('zipcode_name')){
			
		} else {		
			$new_input['zipcode'] = array('123','456','678');
			set_transient('zipcode_name',$new_input);
			
		}
	}

}
function add_submenu_manage_user_setting(){
	$manage_user_setting = new Manage_User_Setting();
	
		add_submenu_page(
				'users.php',
				'Manage user setting',
				'Manage User Setting',
				'read',
				'manage_user',
				array($manage_user_setting, 'create_page_manage_user')
		);
}


function register_my_custom_menu_page() {

	add_menu_page( 'custom menu title', 'custom menu', 'manage_options', 'views/taffel-page.php', '' );

}
function form_code(){
	require(MANAGE_USER_PLUGIN_DIR . 'views/taffel-page.php');
}

// Insert the post into the database
add_action( 'admin_menu', 'register_my_custom_menu_page' );
add_action('admin_menu','add_submenu_manage_user_setting');
add_shortcode( 'page', 'form_code' );