<?php

function main_html(){
    $result = deliver_mail();
    switch ($result) {
        case 1:
            form_information();
            break;
        case 2:
            page_false();
            break;
        case 3:
            page_success();
            break;
        default:
            html_form_code();
            break;
    }
    
}

function html_form_code() {
    require(MANAGE_USER_PLUGIN_DIR . 'views/taffel-page.php');
}

function deliver_mail() {
    
    // if the submit button is clicked, send the email
    if ( isset( $_POST['cf-submitted'] ) ) {
        $zipcode = get_transient('zipcode_name')['zipcode'];
        if (in_array($_POST['cf-zipcode'], $zipcode)){
            return 1;
        } else {
            return 2;
        }
    } elseif (isset( $_POST['sendmail_submit'])){
        $result = $_POST['sendmail'];
        
        return 3;
    }
    return 4;
}

function form_information(){
    require(MANAGE_USER_PLUGIN_DIR . 'views/taffel-page-information.php');  
}
function page_success(){
    require(MANAGE_USER_PLUGIN_DIR . 'views/taffel-page-success.php');  
}
function page_false(){
    require(MANAGE_USER_PLUGIN_DIR . 'views/taffel-page-false.php');    
}

function cf_shortcode() {
    ob_start();
    deliver_mail();
    main_html();
    return ob_get_clean();
}
add_shortcode( 'sitepoint_contact_form', 'cf_shortcode' );